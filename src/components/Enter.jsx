import { Container, Typography } from '@material-ui/core';
import React from 'react';
import Button from '@material-ui/core/Button';

const Enter = ()=> {
    return(
        <Button variant="contained" color="primary">
            Вход
        </Button>
    );
}

export default Enter;