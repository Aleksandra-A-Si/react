import { Typography } from '@material-ui/core';
import React from 'react';
import AppLogo from './Rectangle 34.svg';
const Header = () => {
    return (
        <div className='header_wrapper'>
            <Typography className='logoName'>
                EatSmart
            </Typography>
            <img src={AppLogo}/>

        </div>
    );
}

export default Header;