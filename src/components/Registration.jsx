import React from 'react';
import TextField from '@material-ui/core/TextField';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Container, Typography } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(2),
      width: '25ch',
    },
    
  },
  formControl: {
    margin: theme.spacing(2),
    minWidth: 216,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  formContent: {
    width: 600,
    background: 'white'
  },
  inputLabel: {   
          
  },
  headers: {
    marginTop: 50,
    marginBottom: 30,
  },
  buttonSave: {
    marginTop: 50,
    marginRight: 0,
     marginBottom: 30,
  }  
}));

export default function FormPropsTextFields() {
  const classes = useStyles();
  const [gender, setGender, activities, setActivities, typeOfSlimming, setTypeOfSlimming ] = React.useState('');

  const handleChangeGender = (event) => {
    setGender(event.target.value);
  };

  const handleChangeActivities = (event) => {
    setActivities(event.target.value);
  };
  const handleChangeTypeOfSlimming = (event) => {
    setTypeOfSlimming(event.target.value);
  };
  const handleSave = () => {
    console.log({gender})
  };
  return (
    <form className={classes.root} noValidate autoComplete="off">
           
      <Container className={classes.formContent}>
        <Typography className={classes.headers}>Заполните данные о себе:</Typography>
        <Container fixed>
          <InputLabel htmlFor="mail" className={classes.inputLabel}>Почта</InputLabel>
        {/* <TextField
          disabled
          id="outlined-required"
          label="Почта"
          className={classes.inputLabel}          
        />   */}
        <TextField
          required
          id="mail"          
          defaultValue=""
          variant="outlined"
        />
        {/* <TextField
          disabled
          id="outlined-disabled"
          label={gender}
          defaultValue=''
          variant="outlined"
        /> */}
        <InputLabel htmlFor="password" className={classes.inputLabel}>Пароль</InputLabel>

        <TextField
          required
          id="password"          
          type="password"
          autoComplete="current-password"
          variant="outlined"
        />
        <InputLabel htmlFor="userName" className={classes.inputLabel}>Имя</InputLabel>
        <TextField
          required
          id="userName"          
          defaultValue=""
          variant="outlined"
        />
        <InputLabel htmlFor="birthday" className={classes.inputLabel}>Дата рождения</InputLabel>
        <TextField
          required
          id="birthday"          
          type="date"
          defaultValue=""
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
        />
        </Container>
        <Typography className={classes.headers}>Эта информация поможет рассчитать дневную норму калорий для Вас:</Typography>
        <Container fixed>
          <InputLabel htmlFor="height" className={classes.inputLabel}>Рост</InputLabel>
          <TextField
            required
            id="height"            
            type="number"            
            variant="outlined"
          />
          <InputLabel htmlFor="weight" className={classes.inputLabel}>Вес</InputLabel>
          <TextField
            required
            id="weight"            
            type="number"            
            variant="outlined"
          />
          <InputLabel htmlFor="gender" className={classes.inputLabel}>Пол</InputLabel>
          <FormControl required variant="outlined" className={classes.formControl}>            
            <Select
              labelId="gender"
              id="demo-simple-select-outlined"
              value={gender}
              onChange={handleChangeGender}
              label="Gender"
            >
              <MenuItem value={'женский'}>Женский</MenuItem>
              <MenuItem value={'мужской'}>Мужской</MenuItem>              
            </Select>
          </FormControl>
          <InputLabel htmlFor="activities" className={classes.inputLabel}>Активность</InputLabel>
          <FormControl required variant="outlined" className={classes.formControl}>            
            <Select
              labelId="activities"
              id="demo-simple-select-outlined"
              value={activities}
              onChange={handleChangeActivities}
              label="Activities"
            >
              <MenuItem value={1.2}>Нет нагрузок, сидячая работа</MenuItem>
              <MenuItem value={1.375}>Легкая нагрузка, 1-3 раза в неделю</MenuItem>
              <MenuItem value={1.55}>Средняя нагрузка, 3-4 раза в неделю</MenuItem>
              <MenuItem value={1.725}>Высокая нагрузка, 5-6 раз в неделю</MenuItem>
              <MenuItem value={1.9}>Повышенная нагрузка, каждый день</MenuItem>
            </Select>
          </FormControl>
        </Container> 
        <Typography className={classes.headers}>Если вы желаете расчитать норму калорий для похудения, выберите тип:</Typography>
        <Container fixed>
          <InputLabel htmlFor="typeOfSlimming" className={classes.inputLabel}>Тип похудения</InputLabel>
          <FormControl required variant="outlined" className={classes.formControl}>            
            <Select
              labelId="typeOfSlimming"
              id="demo-simple-select-outlined"
              value={typeOfSlimming}
              onChange={handleChangeTypeOfSlimming}
              label="Activities"
            >
              <MenuItem value={250}>Безопасное</MenuItem>
              <MenuItem value={500}>Экстремальное</MenuItem>              
            </Select>
          </FormControl>
        </Container>
        
      <input type="submit" value="Сохранить" />
      </Container>
    </form>
  );
}